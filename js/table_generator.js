function createFormElements()
{
    //storing html elements into variables and adding evenlisteners to them
    //the event listeners call generate table every time there is a change
    let row_count = document.getElementById("row_count");
    row_count.addEventListener("change", generateTable);

    let col_count = document.getElementById("col_count");
    col_count.addEventListener("change", generateTable);

    let table_width = document.getElementById("table_width");
    table_width.addEventListener("change", generateTable);

    let text_color = document.getElementById("text_color");
    text_color.addEventListener("change", generateTable);

    let background_color = document.getElementById("background_color");
    background_color.addEventListener("change", generateTable);

    let border_width = document.getElementById("border_width");
    border_width.addEventListener("change", generateTable);

    let border_color = document.getElementById("border_color");
    border_color.addEventListener("change", generateTable);
}

function generateTable()
{
    //declaring and storing variable needed for the table
    let table_render_space = document.getElementById("table_render_space");
    let table = document.createElement('table');
    let rows = document.getElementById("row_count").value;
    let columns = document.getElementById('col_count').value;

    //removing table if there already is one
    while (table_render_space.lastChild) {
        table_render_space.removeChild(table_render_space.lastChild);
    }
    //adding the table to html
    table_render_space.appendChild(table);
    
    //creating table according to the inputs using nested loops
    let cell_num = "";
    for(let i = 0; i < rows; i++)
    {
        let row = document.createElement('tr');
        table.appendChild(row);
        for(let j = 0; j < columns; j++ )
        {
            row.appendChild(document.createElement('td'));
            cell_num = "";
            cell_num += i;
            cell_num += j;
            row.cells[j].appendChild(document.createTextNode(cell_num));
        }
        
    }

    //changing the styling of the table according to the inputs
    let text_color = document.getElementById("text_color").value;
    changeTagTextColor(table, text_color );

    let bgrd_color = document.getElementById("background_color").value;
    let tds = table.getElementsByTagName("td");
    let brdr_color = document.getElementById("border_color").value;
    let table_width = document.getElementById("table_width").value;
    let border_width = document.getElementById("border_width").value;
    changeTagWidth(table, table_width);


    for(let i = 0; i < tds.length; i++)
    {
        changeTagBackground(tds[i], bgrd_color );
        changeTagBorderColor(tds[i], brdr_color);
        changeTagBorderWidth(tds[i], border_width);
    }

    
    //calling helper method generate area that creats the text area
    generateTextArea();
}

function generateTextArea()
{
    //declaring variables needed for the html text area
    let rows = document.getElementById("row_count").value;
    let columns = document.getElementById("col_count").value;

    let textArea = document.createElement("textarea");
    let html_area = document.getElementById("table_html_space");

    //deleting the text area if it already exists
    while (html_area.lastChild)
    {
        html_area.removeChild(html_area.lastChild);
    }
    //adding new text area
    html_area.appendChild(textArea);

    //building a string build up that would include the html for that table
    let string_build = "<table> \n";

    //using nested loops for the text content
    for(let i = 0; i < rows; i++)
    {
        string_build += "   <tr> \n";
        for(let j = 0; j < columns; j++)
        {
            string_build += "       <td> " + i +j + " </td> \n" ;
        }
        string_build += "   </tr> \n";
    }
    string_build += "</table>";

    //finally adding the string to the text area
    textArea.textContent = string_build;


}
//calling the methods once everything has been parsed
document.addEventListener("DOMContentLoaded", createFormElements);
document.addEventListener("DOMContentLoaded", generateTable);