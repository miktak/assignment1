function changeTagTextColor(elem, color)
{
    elem.style.color = color;
}

function changeTagBackground(elem, color)
{
    elem.style.backgroundColor = color;
}

function changeTagWidth(elem, width)
{
    elem.style.width = width + "px";
}

function changeTagBorderColor(elem, color)
{
    elem.style.borderColor = color;
}

function changeTagBorderWidth(elem, width)
{
    elem.style.borderWidth = width + "px";
}